from flask import Flask, jsonify
import requests

app = Flask(__name__)

API_KEY = 'c3b40bad7533983514c44849cb17cbab'

@app.route('/weather/<city>')
def get_weather(city):
    url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={API_KEY}&units=metric'
    response = requests.get(url)
    weather_data = response.json()
    return jsonify(weather_data)

@app.route('/api')
def api():
    with open('data.json', mode='r') as my_file:
        text = my_file.read()
        return text